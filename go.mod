module gitlab.com/e-capture/ecatch-ecm/ecmconfiguration

go 1.13

require (
	gitlab.com/e-capture/ecatch-ecm/ecmauthentication v0.0.0
	gitlab.com/e-capture/ecatch-ecm/majosystem v0.0.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/lib/pq v1.3.0
)

replace (
	gitlab.com/e-capture/ecatch-ecm/ecmauthentication => ../ecmauthentication
	gitlab.com/e-capture/ecatch-ecm/majosystem => ../majosystem/v2
)
