package main

import (
	"gitlab.com/e-capture/ecatch-ecm/ecmconfiguration/router"
)

func main() {
	// Este servicio se estará ejecutando cada minuto para saber
	// qué procesos deben ejecutarse y llamar su respectiva ejecución.
	//scheduler.Schedule()

	// Servicio WEB
	router.StartService()
}
