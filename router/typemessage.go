package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/ecmconfiguration/cfg/typemessages"
)

func typemessagesRoute(e *echo.Echo) {
	r := e.Group("/api/v1/typemessages", user.ValidateJWT)

	r.POST("", typemessages.Create)
	r.GET("", typemessages.GetAll)
	r.GET("/:id", typemessages.GetByID)
	r.PUT("/:id", typemessages.Update)
	r.DELETE("/:id", typemessages.Delete)
}
