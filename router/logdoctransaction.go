package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_database/doctransaction"
)

func doctransactionRoute(e *echo.Echo) {
	const route = "/api/v1/doctransaction-admin"
	s := user.Scope{route}
	r := e.Group(route, user.ValidateJWT)
	r.Use(s.ValidatePermissions)

	r.POST("", doctransaction.Create)
	r.GET("", doctransaction.GetAll)
	r.GET("/:id", doctransaction.GetByID)
	r.PUT("/:id", doctransaction.Update)
	r.DELETE("/:id", doctransaction.Delete)
	r.GET("/id/:id", doctransaction.GetByDocumentID)

}

func doctransactionRouteClient(e *echo.Echo) {
	r := e.Group("/api/v1/doctransaction", user.ValidateJWT)

	r.GET("/last", doctransaction.GetLastActionByUserID)
	r.GET("/history/:id", doctransaction.GetDocumentHistory)
}
