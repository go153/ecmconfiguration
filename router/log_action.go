package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_database/action"
)

func actionRoute(e *echo.Echo) {
	r := e.Group("/api/v1/action", user.ValidateJWT)

	r.POST("", action.Create)
	r.GET("", action.GetAll)
	r.GET("/:id", action.GetByID)
	r.PUT("/:id", action.Update)
	r.DELETE("/:id", action.Delete)
}
