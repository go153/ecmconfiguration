package router

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"
)

func StartService() {

	c := configuration.FromFile()

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: c.GetAllowedDomains(),
	}))

	configRoute(e)
	endpointRoute(e)
	actionRoute(e)
	doctransactionRoute(e)
	doctransactionRouteClient(e)
	usrloggeduserRoute(e)
	wfdocumentRoute(e)
	messagesRoute(e)
	networkRoute(e)
	processFileRoute(e)
	schedulerServiceRoute(e)
	schedulerRoute(e)
	typemessagesRoute(e)
	document_assignedRoute(e)
	traceRoute(e)
	// Rutas para validar estado del servicio
	healthRoute(e)

	// Inicia el servidor
	e.Logger.Fatal(e.Start(":" + c.AppPort))
}
