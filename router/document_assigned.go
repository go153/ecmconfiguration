package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_database/document_assigned"
)

func document_assignedRoute(e *echo.Echo) {
	r := e.Group("/api/v1/document_assigned", user.ValidateJWT)

	r.POST("", document_assigned.Create)
	r.GET("", document_assigned.GetAll)
	r.GET("/:id", document_assigned.GetByID)
	r.PUT("/:id", document_assigned.Update)
	r.DELETE("/:id", document_assigned.Delete)
}
