package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/ecmconfiguration/cfg/endpoint"
)

func endpointRoute(e *echo.Echo) {
	r := e.Group("/api/v1/endpoint", user.ValidateJWT)

	r.POST("", endpoint.Create)
	r.GET("", endpoint.GetAll)
	r.GET("/:id", endpoint.GetByID)
	r.PUT("/:id", endpoint.Update)
	r.DELETE("/:id", endpoint.Delete)
}
