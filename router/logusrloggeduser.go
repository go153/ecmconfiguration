package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_database/usrloggeduser"
)

func usrloggeduserRoute(e *echo.Echo) {
	r := e.Group("/api/v1/usrloggeduser", user.ValidateJWT)

	r.POST("", usrloggeduser.Create)
	r.GET("", usrloggeduser.GetAll)
	r.GET("/:id", usrloggeduser.GetByID)
	r.PUT("/:id", usrloggeduser.Update)
	r.DELETE("/:id", usrloggeduser.Delete)
}
