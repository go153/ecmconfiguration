package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/config"
)

func configRoute(e *echo.Echo) {
	r := e.Group("/api/v1/config", user.ValidateJWT)

	r.POST("", config.Create)
	r.GET("", config.GetAll)
	r.GET("/:id", config.GetByID)
	r.PUT("/:id", config.Update)
	r.DELETE("/:id", config.Delete)
	r.GET("/parameter/:param", config.GetByParameter)
}
