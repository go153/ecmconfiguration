package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/ecmconfiguration/cfg/messages"
)

func messagesRoute(e *echo.Echo) {
	r := e.Group("/api/v1/messages", user.ValidateJWT)

	r.POST("", messages.Create)
	r.GET("", messages.GetAllEx)
	r.GET("/:id", messages.GetByID)
	r.PUT("/:id", messages.Update)
	r.DELETE("/:id", messages.Delete)

	s := e.Group("/api/v1/msg")

	s.POST("", messages.Create)
	s.GET("", messages.GetAllEx)
	s.GET("/:id", messages.GetByID)
	s.PUT("/:id", messages.Update)
	s.DELETE("/:id", messages.Delete)
}
