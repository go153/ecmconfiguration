package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	sys_process_file "gitlab.com/e-capture/ecatch-ecm/ecmconfiguration/cfg/process_file"
)

func processFileRoute(e *echo.Echo) {
	r := e.Group("/api/v1/sys_process_file", user.ValidateJWT)

	r.POST("", sys_process_file.Create)
	r.GET("", sys_process_file.GetAll)
	r.GET("/:id", sys_process_file.GetByID)
	r.PUT("/:id", sys_process_file.Update)
	r.DELETE("/:id", sys_process_file.Delete)
}
