package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmconfiguration/services/scheduler"
)

func schedulerServiceRoute(e *echo.Echo) {
	// TODO agregar la validación del token
	// r := e.Group("/api/v1/scheduler-services", user.ValidateJWT)
	r := e.Group("/api/v1/scheduler-services")

	r.GET("", scheduler.GetProcessScheduler)
}
