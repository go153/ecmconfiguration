package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/ecmconfiguration/cfg/network"
)

func networkRoute(e *echo.Echo) {

	e.GET("/api/v1/networks", network.GetAll)
	r := e.Group("/api/v1/network", user.ValidateJWT)

	r.POST("", network.Create)
	r.GET("/:id", network.GetByID)
	r.PUT("/:id", network.Update)
	r.DELETE("/:id", network.Delete)
}
