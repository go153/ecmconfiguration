package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

func traceRoute(e *echo.Echo) {
	r := e.Group("/api/v1/trace", user.ValidateJWT)

	r.POST("", logger_trace.Create)
	r.GET("", logger_trace.GetAll)
	r.GET("/:id", logger_trace.GetByID)
	r.PUT("/:id", logger_trace.Update)
	r.DELETE("/:id", logger_trace.Delete)
}
