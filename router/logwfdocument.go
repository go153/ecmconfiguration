package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_database/wfdocument"
)

func wfdocumentRoute(e *echo.Echo) {
	r := e.Group("/api/v1/log/wfdocument", user.ValidateJWT)

	r.POST("", wfdocument.Create)
	r.GET("", wfdocument.GetAll)
	r.GET("/:id", wfdocument.GetByID)
	r.PUT("/:id", wfdocument.Update)
	r.DELETE("/:id", wfdocument.Delete)
	r.GET("/id/:id", wfdocument.GetByDocumentID)
}
