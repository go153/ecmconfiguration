package router

import (
	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	sys_scheduler "gitlab.com/e-capture/ecatch-ecm/ecmconfiguration/cfg/scheduler"
)

func schedulerRoute(e *echo.Echo) {
	r := e.Group("/api/v1/sys_scheduler", user.ValidateJWT)

	r.POST("", sys_scheduler.Create)
	r.GET("", sys_scheduler.GetAll)
	r.GET("/:id", sys_scheduler.GetByID)
	r.PUT("/:id", sys_scheduler.Update)
	r.DELETE("/:id", sys_scheduler.Delete)
}
