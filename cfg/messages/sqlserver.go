package messages

import (
	"database/sql"
	"strings"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type sqlserver struct{}

const (
	sqlInsert       = `INSERT INTO cfg.messages (cod,spa, eng, typemsgs_id ) VALUES (@cod,@spa, @eng, @type ) SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	sqlUpdate       = `UPDATE cfg.messages SET cod = @cod, spa = @spa, eng = @eng, typemsgs_id = @type,  updated_at = getdate() WHERE id = @id`
	sqlDelete       = `DELETE FROM cfg.messages WHERE id = @id`
	sqlGetByIDSP    = `SELECT id, cod, spa, eng, spa msg, typemsgs_id,  created_at, updated_at FROM cfg.messages WITH (NOLOCK) WHERE id = @id`
	sqlGetByIDEn    = `SELECT id, cod, spa, eng, eng msg, typemsgs_id,  created_at, updated_at FROM cfg.messages WITH (NOLOCK) WHERE id = @id`
	sqlGetAll       = `SELECT id, cod, spa, eng, '', typemsgs_id,  created_at, updated_at FROM cfg.messages WITH (NOLOCK)`
	sqlGetAllEx     = `SELECT  m.id, m.cod, m.spa, m.eng, '' msg, tm.name, m.created_at, m.updated_at FROM cfg.messages m WITH (NOLOCK) JOIN cfg.typemsgs tm WITH (NOLOCK) on (tm.id = m.typemsgs_id)`
	sqlGetMessageSp = `SELECT m.id, cod message_id, spa message ,tm.name FROM cfg.messages m WITH (NOLOCK) JOIN cfg.typemsgs tm  WITH (NOLOCK) on (tm.id = m.typemsgs_id)`
	sqlGetMessageEn = `SELECT m.id, cod message_id, eng message ,tm.name FROM cfg.messages m WITH (NOLOCK) JOIN cfg.typemsgs tm  WITH (NOLOCK) on (tm.id = m.typemsgs_id)`
)

func (s sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create messages: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		sql.Named("cod", m.Cod),
		sql.Named("spa", m.Spa),
		sql.Named("eng", m.Eng),
		sql.Named("type", m.Type),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create messages: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update messages: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("cod", m.Cod),
		sql.Named("spa", m.Spa),
		sql.Named("eng", m.Eng),
		sql.Named("type", m.Type),
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update messages: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete messages: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete messages: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()
	c := configuration.FromFile()
	var GetMessages string
	switch strings.ToLower(c.AppLang) {
	// español
	case "sp":
		GetMessages = sqlGetByIDSP
		// ingles
	case "en":
		GetMessages = sqlGetMessageEn
	default:
		GetMessages = sqlGetByIDEn
	}
	stmt, err := conn.Prepare(GetMessages)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID messages: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", ID))
	return s.scanRow(row)
}

func (s sqlserver) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll messages: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll messages: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en messages: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,
		&m.Cod,
		&m.Spa,
		&m.Eng,
		&m.Msg,
		&m.Type,
		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo messages: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}

func (s sqlserver) GetMessage() (*Msgs, error) {
	conn := db.GetConnection()
	ms := make(Msgs, 0)
	c := configuration.FromFile()
	var GetMessages string
	switch strings.ToLower(c.AppLang) {
	// español
	case "sp":
		GetMessages = sqlGetMessageSp
		// ingles
	case "en":
		GetMessages = sqlGetMessageEn
	default:
		GetMessages = sqlGetMessageSp
	}

	stmt, err := conn.Prepare(GetMessages)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetMessage messages: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetMessage messages: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m := &Msg{}
		err := rows.Scan(
			&m.ID,
			&m.Cod,
			&m.Msg,
			&m.Type,
		)
		if err != nil {
			logger_trace.Error.Printf("escaneando el modelo messages: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}
	return &ms, nil
}

func (s sqlserver) GetAllEx() (*MsgExs, error) {
	conn := db.GetConnection()
	ms := make(MsgExs, 0)
	//c := configuration.FromFile()

	stmt, err := conn.Prepare(sqlGetAllEx)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetMessage messages: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetMessage messages: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m := &MsgEx{}
		err := rows.Scan(
			&m.ID,
			&m.Cod,
			&m.Spa,
			&m.Eng,
			&m.Msg,
			&m.Type,
			&m.CreatedAt,
			&m.UpdatedAt,
		)
		if err != nil {
			logger_trace.Error.Printf("escaneando el modelo messages: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}
	return &ms, nil
}
