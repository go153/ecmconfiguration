package messages

import (
	"strings"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type orcl struct{}

const (
	orclInsert       = `INSERT INTO sys_messages (cod,spa, eng, typemsgs_id ) VALUES (:1,:2, :3, :4 )`
	orclUpdate       = `UPDATE sys_messages SET cod = :1, spa = :2, eng = :3, typemsgs_id = :4,  updated_at = getdate() WHERE id = :5`
	orclDelete       = `DELETE FROM sys_messages WHERE id = :1`
	orclGetByID      = `SELECT id, cod, spa, eng, typemsgs_id,  created_at, updated_at FROM sys_messages WITH (NOLOCK) WHERE id = :1`
	orclGetAll       = `SELECT id, cod, spa, eng, typemsgs_id,  created_at, updated_at FROM sys_messages WITH (NOLOCK)`
	orclGetAllEx     = `SELECT  m.id, m.cod, m.spa, m.eng, '' msg, tm.name, m.created_at, m.updated_at FROM sys_messages m WITH (NOLOCK) JOIN sys_typemsgs tm WITH (NOLOCK) on (tm.id = m.typemsgs_id)`
	orclGetMessageSp = `SELECT m.id, cod messagge_id, spa message ,tm.name FROM sys_messages m WITH (NOLOCK) JOIN sys_typemsgs tm  WITH (NOLOCK) on (tm.id = m.typemsgs_id)`
	orclGetMessageEn = `SELECT m.id, cod messagge_id, eng message ,tm.name FROM sys_messages m WITH (NOLOCK) JOIN sys_typemsgs tm  WITH (NOLOCK) on (tm.id = m.typemsgs_id)`
)

func (o orcl) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create messages: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		&m.Cod,
		&m.Spa,
		&m.Eng,
		&m.Type,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create messages: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (o orcl) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update messages: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt, &m.Cod, &m.Spa, &m.Eng, &m.Type, ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update messages: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (o orcl) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete messages: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete messages: %v", err)
		return err
	}

	return nil
}

func (o orcl) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID messages: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(ID)
	return o.scanRow(row)
}

func (o orcl) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(orclGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll messages: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll messages: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := o.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en messages: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (o orcl) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,
		&m.Cod,
		&m.Spa,
		&m.Eng,
		&m.Type,
		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo messages: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}

func (o orcl) GetMessage() (*Msgs, error) {
	conn := db.GetConnection()
	ms := make(Msgs, 0)
	c := configuration.FromFile()
	var GetMessages string
	switch strings.ToLower(c.AppLang) {
	// español
	case "sp":
		GetMessages = sqlGetMessageSp
		// ingles
	case "en":
		GetMessages = sqlGetMessageEn
	default:
		GetMessages = sqlGetMessageSp
	}

	stmt, err := conn.Prepare(GetMessages)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetMessage messages: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetMessage messages: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m := &Msg{}
		err := rows.Scan(
			&m.ID,
			&m.Cod,
			&m.Msg,
			&m.Type,
		)
		if err != nil {
			logger_trace.Error.Printf("escaneando el modelo messages: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}
	return &ms, nil
}

func (s orcl) GetAllEx() (*MsgExs, error) {
	conn := db.GetConnection()
	ms := make(MsgExs, 0)
	//c := configuration.FromFile()

	stmt, err := conn.Prepare(orclGetAllEx)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetMessage messages: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetMessage messages: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m := &MsgEx{}
		err := rows.Scan(
			&m.ID,
			&m.Cod,
			&m.Spa,
			&m.Eng,
			&m.Msg,
			&m.Type,
			&m.CreatedAt,
			&m.UpdatedAt,
		)
		if err != nil {
			logger_trace.Error.Printf("escaneando el modelo messages: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}
	return &ms, nil
}
