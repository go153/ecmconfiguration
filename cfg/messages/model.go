package messages

import "time"

type Model struct {
	ID        int64     `json:"id"`
	Cod       int64     `json:"cod"`
	Spa       string    `json:"spa"`
	Eng       string    `json:"eng"`
	Msg       string    `json:"msg"`
	Type      int       `json:"type"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type Models []Model

func (m *Model) Create() error {
	return s.Create(m)
}

func (m *Model) Update(ID int64) error {
	return s.Update(ID, m)
}

func (m *Model) Delete(ID int64) error {
	return s.Delete(ID)
}

func (m *Model) GetByID(ID int64) (*Model, error) {
	return s.GetByID(ID)
}

func (m *Model) GetAll() (Models, error) {
	return s.GetAll()
}

func (m *Model) GetMessage() (*Msgs, error) {
	return s.GetMessage()
}

func (m *Model) GetAllEx() (*MsgExs, error) {
	return s.GetAllEx()
}

type Msg struct {
	ID   int64  `json:"id"`
	Cod  int64  `json:"cod"`
	Msg  string `json:"msg"`
	Type string `json:"type"`
}

type Msgs []Msg

//
type MsgEx struct {
	ID        int64     `json:"id"`
	Cod       int64     `json:"cod"`
	Spa       string    `json:"spa"`
	Eng       string    `json:"eng"`
	Msg       string    `json:"msg"`
	Type      string    `json:"type"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type MsgExs []MsgEx
