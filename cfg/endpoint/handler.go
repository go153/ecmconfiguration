package endpoint

import (
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"

	"github.com/labstack/echo"

	"gitlab.com/e-capture/ecatch-ecm/majosystem/response"
)

func Create(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	err := c.Bind(&m)
	if err != nil {
		errStr := fmt.Sprintf("estructura no válida: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = m.Create()
	if err != nil {
		errStr := fmt.Sprintf("no se pudo insertar el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 3)
		return c.JSON(http.StatusAccepted, rm)
	}
	rm.Set(false, m, 4)
	return c.JSON(http.StatusCreated, rm)
}

func Update(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	ID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		errStr := "el id debe ser un número entero positivo"
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 17)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = c.Bind(&m)
	if err != nil {
		errStr := fmt.Sprintf("estructura no válida: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, rm)
	}
	m.ID = ID

	err = m.Update(m.ID)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo actualizar el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 18)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, nil, 19)
	return c.JSON(http.StatusOK, rm)
}

func Delete(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	ID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		errStr := "el id debe ser un número entero positivo"
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 17)
		return c.JSON(http.StatusAccepted, rm)
	}

	err = m.Delete(ID)
	if err != nil {
		errStr := fmt.Sprintf("no se eliminó el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 20)
		return c.JSON(http.StatusAccepted, rm)
	}
	rm.Set(false, nil, 28)
	return c.JSON(http.StatusOK, rm)
}

func GetByID(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	ID, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		errStr := "el id debe ser un número entero positivo"
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 17)
		return c.JSON(http.StatusAccepted, rm)
	}

	r, err := m.GetByID(ID)
	if err != nil {
		errStr := fmt.Sprintf("no se pudo consultar el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 22)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, r, 29)
	return c.JSON(http.StatusOK, rm)
}

func GetAll(c echo.Context) error {
	rm := response.Model{}
	m := Model{}

	rs, err := m.GetAll()
	if err != nil {
		errStr := fmt.Sprintf("no se pudo consultar el registro: %v", err)
		logger_trace.Error.Print(errStr)
		rm.Set(true, nil, 22)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, rs, 29)
	return c.JSON(http.StatusOK, rm)
}
