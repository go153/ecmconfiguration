package endpoint

import (
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type pgsql struct{}

const (
	pgsqlInsert  = `INSERT INTO sys_endpoints (name, url, private_key, active, ) VALUES ($0, $1, $2, $3, )`
	pgsqlUpdate  = `UPDATE sys_endpoints SET name = $0, url = $1, private_key = $2, active = $3,  updated_at = getdate() WHERE id = $1`
	pgsqlDelete  = `DELETE FROM sys_endpoints WHERE id = $1`
	pgsqlGetByID = `SELECT id, name, url, private_key, active,  created_at, updated_at FROM sys_endpoints WITH (NOLOCK) WHERE id = $1`
	pgsqlGetAll  = `SELECT id, name, url, private_key, active,  created_at, updated_at FROM sys_endpoints WITH (NOLOCK)`
)

func (p pgsql) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create endpoint: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		&m.Name, &m.Url, &m.PrivateKey, &m.Active,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create endpoint: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (p pgsql) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update endpoint: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		&m.Name, &m.Url, &m.PrivateKey, &m.Active,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update endpoint: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (p pgsql) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete endpoint: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete endpoint: %v", err)
		return err
	}

	return nil
}

func (p pgsql) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID endpoint: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(ID)
	return p.scanRow(row)
}

func (p pgsql) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(pgsqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll endpoint: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll endpoint: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := p.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en endpoint: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (p pgsql) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,

		&m.Name,

		&m.Url,

		&m.PrivateKey,

		&m.Active,

		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo endpoint: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
