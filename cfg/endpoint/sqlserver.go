package endpoint

import (
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type sqlserver struct{}

const (
	sqlInsert    = `INSERT INTO cfg.endpoints (name, url, private_key, active) VALUES (@name, @url, @private_key, @active)  SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	sqlUpdate    = `UPDATE cfg.endpoints SET name = @name, url = @url, private_key = @private_key, active = @active,  updated_at = getdate() WHERE id = @id`
	sqlDelete    = `DELETE FROM cfg.endpoints WHERE id = @id`
	sqlGetByID   = `SELECT id, name, url, private_key, active,  created_at, updated_at FROM cfg.endpoints WITH (NOLOCK) WHERE id = @id`
	sqlGetAll    = `SELECT id, name, url, private_key, active,  created_at, updated_at FROM cfg.endpoints WITH (NOLOCK)`
	sqlGetByName = sqlGetAll + " WHERE name = @name"
)

func (s sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create endpoint: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		sql.Named("name", m.Name),
		sql.Named("url", m.Url),
		sql.Named("private_key", m.PrivateKey),
		sql.Named("active", m.Active),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create endpoint: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update endpoint: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("name", m.Name),
		sql.Named("url", m.Url),
		sql.Named("private_key", m.PrivateKey),
		sql.Named("active", m.Active),
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update endpoint: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete endpoint: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete endpoint: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID endpoint: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", ID))
	return s.scanRow(row)
}

func (s sqlserver) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll endpoint: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll endpoint: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en endpoint: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (s sqlserver) GetByName(n string) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByName)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID endpoint: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("name", n))
	return s.scanRow(row)
}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,
		&m.Name,
		&m.Url,
		&m.PrivateKey,
		&m.Active,
		&cn,
		&un,
	)
	if err == sql.ErrNoRows {
		return nil, err
	}
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo endpoint: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
