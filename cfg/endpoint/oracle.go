package endpoint

import (
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type orcl struct{}

const (
	orclInsert  = `INSERT INTO sys_endpoints (name, url, private_key, active, ) VALUES (:0, :1, :2, :3, )`
	orclUpdate  = `UPDATE sys_endpoints SET name = :0, url = :1, private_key = :2, active = :3,  updated_at = getdate() WHERE id = :1`
	orclDelete  = `DELETE FROM sys_endpoints WHERE id = :1`
	orclGetByID = `SELECT id, name, url, private_key, active,  created_at, updated_at FROM sys_endpoints WITH (NOLOCK) WHERE id = :1`
	orclGetAll  = `SELECT id, name, url, private_key, active,  created_at, updated_at FROM sys_endpoints WITH (NOLOCK)`
)

func (o orcl) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create endpoint: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		&m.Name, &m.Url, &m.PrivateKey, &m.Active,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create endpoint: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (o orcl) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update endpoint: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		&m.Name, &m.Url, &m.PrivateKey, &m.Active,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update endpoint: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (o orcl) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete endpoint: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete endpoint: %v", err)
		return err
	}

	return nil
}

func (o orcl) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID endpoint: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(ID)
	return o.scanRow(row)
}

func (o orcl) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(orclGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll endpoint: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll endpoint: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := o.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en endpoint: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (o orcl) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,

		&m.Name,

		&m.Url,

		&m.PrivateKey,

		&m.Active,

		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo endpoint: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
