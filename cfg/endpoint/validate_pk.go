package endpoint

import (
	"net/http"

	"database/sql"

	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/response"
)

// ValidatePK valida la llave privada del endpoint
type Validation struct {
	Name string
}

// ValidatePK permite validar si la llave privada enviada en el QueryParam
// coincide con la registrada en la base de datos de el endpoint a consultar
func (v *Validation) Validate(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		rm := response.Model{}

		pk := c.QueryParam("privateKey")
		if pk == "" {
			logger_trace.Error.Print("la petición no trae el privateKey")
			rm.Set(true, nil, 110)
			return c.JSON(http.StatusAccepted, rm)
		}

		ep := &Model{}
		ep.Name = v.Name
		ep, err := ep.GetByName()
		if err == sql.ErrNoRows {
			logger_trace.Error.Printf("no se encuentra el endpoint con nombre: %s, %v", v.Name, err)
			rm.Set(true, nil, 95)
			return c.JSON(http.StatusAccepted, rm)
		}
		if err != nil {
			logger_trace.Error.Printf("no se pudo consultar el endpoint en la BD: %s, %v", v.Name, err)
			rm.Set(true, nil, 22)
			return c.JSON(http.StatusAccepted, rm)
		}

		if ep.PrivateKey != pk {
			logger_trace.Error.Printf("la llave privada del endpoint no es la correcta: %s", v.Name)
			rm.Set(true, nil, 112)
			return c.JSON(http.StatusAccepted, rm)
		}

		if !ep.Active {
			logger_trace.Error.Printf("el endpoint no se encuentra activo para procesar: %s", v.Name)
			rm.Set(true, nil, 111)
			return c.JSON(http.StatusAccepted, rm)
		}

		return next(c)
	}
}
