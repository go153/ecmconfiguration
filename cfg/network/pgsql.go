package network

import (
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type pgsql struct{}

const (
	pgsqlInsert  = `INSERT INTO sys_network (ecm, ip, port, path ) VALUES ($1, $2, $3, $4 )`
	pgsqlUpdate  = `UPDATE sys_network SET ecm = $1, ip = $2, port = $3, path = $4  updated_at = getdate() WHERE id = $4`
	pgsqlDelete  = `DELETE FROM sys_network WHERE id = $1`
	pgsqlGetByID = `SELECT id, ecm, ip, port, path, created_at, updated_at FROM sys_network WITH (NOLOCK) WHERE id = $1`
	pgsqlGetAll  = `SELECT id, ecm, ip, port, path, created_at, updated_at FROM sys_network WITH (NOLOCK)`
)

func (p pgsql) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create network: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		&m.Ecm, &m.Ip, &m.Port, &m.Path,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create network: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (p pgsql) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update network: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		&m.Ecm, &m.Ip, &m.Port, &m.Path,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update network: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (p pgsql) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete network: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete network: %v", err)
		return err
	}

	return nil
}

func (p pgsql) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID network: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(ID)
	return p.scanRow(row)
}

func (p pgsql) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(pgsqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll network: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll network: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := p.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en network: %v", err)
			return ms, err
		}

		ms = append(ms, *m)
	}

	return ms, nil
}

func (p pgsql) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,

		&m.Ecm,

		&m.Ip,

		&m.Port,

		&m.Path,

		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo network: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
