package sys_scheduler

import (
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type pgsql struct{}

const (
	pgsqlInsert  = `INSERT INTO sys_schedulers (service_name, sys_endpoint_id, frequency, begin_at, end_at, enabled, ) VALUES ($0, $1, $2, $3, $4, $5, )`
	pgsqlUpdate  = `UPDATE sys_schedulers SET service_name = $0, sys_endpoint_id = $1, frequency = $2, begin_at = $3, end_at = $4, enabled = $5,  updated_at = getdate() WHERE id = $1`
	pgsqlDelete  = `DELETE FROM sys_schedulers WHERE id = $1`
	pgsqlGetByID = `SELECT id, service_name, sys_endpoint_id, frequency, begin_at, end_at, enabled,  created_at, updated_at FROM sys_schedulers WITH (NOLOCK) WHERE id = $1`
	pgsqlGetAll  = `SELECT id, service_name, sys_endpoint_id, frequency, begin_at, end_at, enabled,  created_at, updated_at FROM sys_schedulers WITH (NOLOCK)`
)

func (p pgsql) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create sys_scheduler: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		&m.ServiceName, &m.EndpointID, &m.Frequency, &m.BeginAt, &m.EndAt, &m.Enabled,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create sys_scheduler: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (p pgsql) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update sys_scheduler: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		&m.ServiceName, &m.EndpointID, &m.Frequency, &m.BeginAt, &m.EndAt, &m.Enabled,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update sys_scheduler: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (p pgsql) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete sys_scheduler: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete sys_scheduler: %v", err)
		return err
	}

	return nil
}

func (p pgsql) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(pgsqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID sys_scheduler: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(ID)
	return p.scanRow(row)
}

func (p pgsql) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(pgsqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll sys_scheduler: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll sys_scheduler: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := p.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en sys_scheduler: %v", err)
			return ms, err
		}

		ms = append(ms, m)
	}

	return ms, nil
}

func (p pgsql) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,
		&m.ServiceName,
		&m.EndpointID,
		&m.Frequency,
		&m.BeginAt,
		&m.EndAt,
		&m.Enabled,
		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo sys_scheduler: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
