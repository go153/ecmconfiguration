package sys_scheduler

import (
	"database/sql"

	"time"

	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type sqlserver struct{}

const (
	sqlInsert              = `INSERT INTO cfg.schedulers (service_name, sys_endpoint_id, frequency, day_of_week, day_of_month, begin_at, end_at, enabled, last_execution) VALUES (@service_name, @sys_endpoint_id, @frequency, @day_of_week, @day_of_month, @begin_at, @end_at, @enabled, @last_execution) SELECT ID = convert(bigint, SCOPE_IDENTITY()) `
	sqlUpdate              = `UPDATE cfg.schedulers SET service_name = @service_name, sys_endpoint_id = @sys_endpoint_id, frequency = @frequency, day_of_week = @day_of_week, day_of_month = @day_of_month,  begin_at = @begin_at, end_at = @end_at, enabled = @enabled, last_execution = @last_execution, updated_at = getdate() WHERE id = @id`
	sqlDelete              = `DELETE FROM cfg.schedulers WHERE id = @id`
	sqlGetByID             = `SELECT id, service_name, sys_endpoint_id, frequency, day_of_week, day_of_month, begin_at, end_at, enabled, last_execution, created_at, updated_at FROM cfg.schedulers WITH (NOLOCK) WHERE id = @id`
	sqlGetAll              = `SELECT id, service_name, sys_endpoint_id, frequency, day_of_week, day_of_month, begin_at, end_at, enabled, last_execution, created_at, updated_at FROM cfg.schedulers WITH (NOLOCK)`
	sqlUpdateLastExecution = "UPDATE cfg.schedulers SET last_execution = @last_execution, updated_at = getdate() WHERE id = @id"
)

func (s sqlserver) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create sys_scheduler: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		sql.Named("service_name", m.ServiceName),
		sql.Named("sys_endpoint_id", m.EndpointID),
		sql.Named("frequency", m.Frequency),
		sql.Named("day_of_week", m.DayOfWeek),
		sql.Named("day_of_month", m.DayOfMonth),
		sql.Named("begin_at", m.BeginAt),
		sql.Named("end_at", m.EndAt),
		sql.Named("enabled", m.Enabled),
		sql.Named("last_execution", m.LastExecution),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create sys_scheduler: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update sys_scheduler: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("service_name", m.ServiceName),
		sql.Named("sys_endpoint_id", m.EndpointID),
		sql.Named("frequency", m.Frequency),
		sql.Named("day_of_week", m.DayOfWeek),
		sql.Named("day_of_month", m.DayOfMonth),
		sql.Named("begin_at", m.BeginAt),
		sql.Named("end_at", m.EndAt),
		sql.Named("enabled", m.Enabled),
		sql.Named("last_execution", m.LastExecution),
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update sys_scheduler: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (s sqlserver) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete sys_scheduler: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete sys_scheduler: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID sys_scheduler: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", ID))
	return s.scanRow(row)
}

func (s sqlserver) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll sys_scheduler: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll sys_scheduler: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en sys_scheduler: %v", err)
			return ms, err
		}

		ms = append(ms, m)
	}

	return ms, nil
}

// UpdateLastExecution actualiza la última fecha de ejecución del scheduler
func (s sqlserver) UpdateLastExecution(ID int64, t *time.Time) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(sqlUpdateLastExecution)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en UpdateLastExecution sys_scheduler: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("last_execution", t),
		sql.Named("id", ID),
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en UpdateLastExecution sys_scheduler: %v", err)
		return err
	}

	return nil
}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	le := pq.NullTime{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,
		&m.ServiceName,
		&m.EndpointID,
		&m.Frequency,
		&m.DayOfWeek,
		&m.DayOfMonth,
		&m.BeginAt,
		&m.EndAt,
		&m.Enabled,
		&le,
		&cn,
		&un,
	)
	if err == sql.ErrNoRows {
		return nil, err
	}
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo sys_scheduler: %v", err)
		return nil, err
	}
	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	m.LastExecution = le.Time

	return m, nil
}
