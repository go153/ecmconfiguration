package sys_scheduler

import (
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type orcl struct{}

const (
	orclInsert  = `INSERT INTO sys_schedulers (service_name, sys_endpoint_id, frequency, begin_at, end_at, enabled, ) VALUES (:0, :1, :2, :3, :4, :5, )`
	orclUpdate  = `UPDATE sys_schedulers SET service_name = :0, sys_endpoint_id = :1, frequency = :2, begin_at = :3, end_at = :4, enabled = :5,  updated_at = getdate() WHERE id = :1`
	orclDelete  = `DELETE FROM sys_schedulers WHERE id = :1`
	orclGetByID = `SELECT id, service_name, sys_endpoint_id, frequency, begin_at, end_at, enabled,  created_at, updated_at FROM sys_schedulers WITH (NOLOCK) WHERE id = :1`
	orclGetAll  = `SELECT id, service_name, sys_endpoint_id, frequency, begin_at, end_at, enabled,  created_at, updated_at FROM sys_schedulers WITH (NOLOCK)`
)

func (o orcl) Create(m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclInsert)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Create sys_scheduler: %v", err)
		return err
	}
	defer stmt.Close()

	ID, err := db.ExecGettingID(
		stmt,
		&m.ServiceName, &m.EndpointID, &m.Frequency, &m.BeginAt, &m.EndAt, &m.Enabled,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Create sys_scheduler: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (o orcl) Update(ID int64, m *Model) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclUpdate)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Update sys_scheduler: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		&m.ServiceName, &m.EndpointID, &m.Frequency, &m.BeginAt, &m.EndAt, &m.Enabled,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Update sys_scheduler: %v", err)
		return err
	}
	m.ID = ID

	return nil
}

func (o orcl) Delete(ID int64) error {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclDelete)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en Delete sys_scheduler: %v", err)
		return err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		ID,
	)
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en Delete sys_scheduler: %v", err)
		return err
	}

	return nil
}

func (o orcl) GetByID(ID int64) (*Model, error) {
	conn := db.GetConnection()

	stmt, err := conn.Prepare(orclGetByID)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetByID sys_scheduler: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(ID)
	return o.scanRow(row)
}

func (o orcl) GetAll() (Models, error) {
	conn := db.GetConnection()
	ms := make(Models, 0)

	stmt, err := conn.Prepare(orclGetAll)
	if err != nil {
		logger_trace.Error.Printf("preparando la consulta en GetAll sys_scheduler: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando la consulta en GetAll sys_scheduler: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := o.scanRow(rows)
		if err != nil {
			logger_trace.Error.Printf("escaneando el registro en sys_scheduler: %v", err)
			return ms, err
		}

		ms = append(ms, m)
	}

	return ms, nil
}

func (o orcl) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	cn := pq.NullTime{}
	un := pq.NullTime{}

	err := rs.Scan(
		&m.ID,
		&m.ServiceName,
		&m.EndpointID,
		&m.Frequency,
		&m.BeginAt,
		&m.EndAt,
		&m.Enabled,
		&cn,
		&un,
	)
	if err != nil {
		logger_trace.Error.Printf("escaneando el modelo sys_scheduler: %v", err)
		return nil, err
	}

	m.CreatedAt = cn.Time
	m.UpdatedAt = un.Time
	return m, nil
}
