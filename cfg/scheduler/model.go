package sys_scheduler

import "time"

type Model struct {
	ID            int64     `json:"id"`
	ServiceName   string    `json:"service_name"`
	EndpointID    int64     `json:"sys_endpoint_id"`
	Frequency     int       `json:"frequency"`
	DayOfWeek     string    `json:"day_of_week"`
	DayOfMonth    string    `json:"day_of_month"`
	BeginAt       time.Time `json:"begin_at"`
	EndAt         time.Time `json:"end_at"`
	Enabled       bool      `json:"enabled"`
	LastExecution time.Time `json:"last_execution"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
}

type Models []*Model

func (m *Model) Create() error {
	return s.Create(m)
}

func (m *Model) Update(ID int64) error {
	return s.Update(ID, m)
}

func (m *Model) Delete(ID int64) error {
	return s.Delete(ID)
}

func (m *Model) GetByID(ID int64) (*Model, error) {
	return s.GetByID(ID)
}

func (m *Model) GetAll() (Models, error) {
	return s.GetAll()
}

func (m *Model) UpdateLastExecution(t *time.Time) error {
	return s.UpdateLastExecution(m.ID, t)
}
