package scheduler

import (
	"time"

	"database/sql"

	"net/http"

	"strconv"
	"strings"

	"gitlab.com/e-capture/ecatch-ecm/ecmconfiguration/cfg/endpoint"
	sys_scheduler "gitlab.com/e-capture/ecatch-ecm/ecmconfiguration/cfg/scheduler"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/httpclient"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

func Schedule() {
	ticker := time.NewTicker(1 * time.Minute)
	go processSchedule(ticker)
}

func processSchedule(ticker *time.Ticker) {
	for range ticker.C {
		now := time.Now()
		for _, v := range process {
			go processExecute(v, &now)
		}
	}
}

func processExecute(m *sys_scheduler.Model, now *time.Time) {
	if !m.Enabled {
		return
	}

	if !todayIsScheduler(m, now) {
		return
	}

	if !frequencyReady(m, now) {
		return
	}

	logger_trace.Trace.Printf("ejecutando %s, llamando a %d", m.ServiceName, m.EndpointID)
	ep := &endpoint.Model{}
	ep, err := ep.GetByID(m.EndpointID)
	if err == sql.ErrNoRows {
		logger_trace.Error.Printf("no se encontró un endpoint con el ID: %d, %v", m.EndpointID, err)
		return
	}
	if err != nil {
		logger_trace.Error.Printf("no se pudo consultar el endpoint con el ID: %d, %v", m.EndpointID, err)
		return
	}

	cli, err := httpclient.DoRequestWithPrivateKey(http.MethodGet, ep.Url, nil, ep.PrivateKey)
	if err != nil {
		logger_trace.Error.Printf("no se pudo hacer la petición al endpoint: %s, %s, %v", ep.Name, ep.Url, err)
		return
	}

	m.LastExecution = *now
	err = m.UpdateLastExecution(&m.LastExecution)
	if err != nil {
		logger_trace.Error.Printf("no se pudo actualizar la fecha de ejecución del scheduler: %v", err)
	}

	logger_trace.Trace.Print(string(cli))
}

func todayIsScheduler(m *sys_scheduler.Model, now *time.Time) bool {
	// Están habilitados todos los días de la semana y del mes
	if m.DayOfWeek == "*" && m.DayOfMonth == "*" {
		return true
	}

	// Si el día del MES está programado se ejecuta sin importar el día de la semana
	if m.DayOfMonth != "*" && m.DayOfMonth != "" {
		for _, v := range getDaysFromString(m.DayOfMonth) {
			if now.Day() == v {
				return true
			}
		}

		return false
	}

	// Si es el día de la semana a ejecutar
	if m.DayOfWeek == "*" {
		return true
	}

	for _, v := range getDaysFromString(m.DayOfWeek) {
		if int(now.Weekday()) == v {
			return true
		}
	}

	return false
}

// getDaysFromString obtiene el string de el campo DayOfMonth y lo separa y lo convierte en enteros
func getDaysFromString(s string) []int {
	r := make([]int, 0)
	ss := strings.Split(s, ",")
	for _, v := range ss {
		t, err := strconv.Atoi(v)
		if err != nil {
			logger_trace.Error.Printf("no se pudo convertir a entero en getDaysFromString (scheduler): %v", err)
			continue
		}
		r = append(r, t)
	}

	return r
}

func frequencyReady(m *sys_scheduler.Model, now *time.Time) bool {
	if m.Frequency == 0 && now.YearDay() == m.LastExecution.YearDay() {
		return false
	}

	begin := time.Date(0, 0, 0, m.BeginAt.Hour(), m.BeginAt.Minute(), m.BeginAt.Second(), 0, time.UTC)
	end := time.Date(0, 0, 0, m.EndAt.Hour(), m.EndAt.Minute(), m.EndAt.Second(), 0, time.UTC)
	nowDate := time.Date(0, 0, 0, now.Hour(), now.Minute(), now.Second(), 0, time.UTC)
	diffBegin := nowDate.Sub(begin)
	diffEnd := nowDate.Sub(end)
	if diffBegin >= 0 && diffEnd < 0 {
		// Si no hay fecha de última ejecución, está listo para ejecutarse
		if m.LastExecution.IsZero() {
			return true
		}

		// Se compara la fecha de la última ejecución con "ahora" y verificar si es mayor o igual a la frecuencia
		logger_trace.Trace.Print("diferencia ultima ejecución", now.Sub(m.LastExecution).Minutes())
		if now.Sub(m.LastExecution).Minutes() >= float64(m.Frequency) {
			return true
		}
	}

	return false
}
