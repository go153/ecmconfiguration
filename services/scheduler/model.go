package scheduler

import (
	sys_scheduler "gitlab.com/e-capture/ecatch-ecm/ecmconfiguration/cfg/scheduler"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

var process []*sys_scheduler.Model

func init() {
	err := GetProcess()
	if err != nil {
		logger_trace.Error.Printf("no se pudo consultar los procesos a ejecutar: %v", err)
	}
}

func GetProcess() error {
	var err error
	m := &sys_scheduler.Model{}
	process, err = m.GetAll()
	if err != nil {
		logger_trace.Error.Printf("no se pudo leer la tabla scheduler: %v", err)
		return err
	}

	return nil
}
