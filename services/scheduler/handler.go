package scheduler

import (
	"net/http"

	"github.com/labstack/echo"
	sys_scheduler "gitlab.com/e-capture/ecatch-ecm/ecmconfiguration/cfg/scheduler"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/response"
)

func GetProcessScheduler(c echo.Context) error {
	rm := response.Model{}

	process = []*sys_scheduler.Model{}
	err := GetProcess()
	if err != nil {
		logger_trace.Error.Printf("no se pudo consultar los procesos a ejecutar de sys_schedulers: %v", err)
		rm.Set(true, nil, 106)
		return c.JSON(http.StatusAccepted, rm)
	}

	rm.Set(false, nil, 29)
	return c.JSON(http.StatusOK, rm)
}
