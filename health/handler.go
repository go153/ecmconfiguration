package health

import (
	"net/http"

	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"

	"gitlab.com/e-capture/ecatch-ecm/majosystem/response""
	"github.com/labstack/echo
)

func Health(c echo.Context) error {
	rm := response.Model{}
	type health struct {
		Name string `json:"name"`
		Age  int    `json:"age"`
	}
	h := &health{}

	err := c.Bind(h)
	if err != nil {
		logger_trace.Error.Printf("la estructura no es correcta: %v", err)
		rm.Set(true, nil, 1)
	}

	c.Set("bodyRequest", h)
	rm.Set(false, h, 2)
	return c.JSON(http.StatusOK, rm)
}
